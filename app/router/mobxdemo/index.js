import {observable, autorun, computed} from 'mobx';
import React from 'react'

class MobxDemo extends React.Component {
  constructor(props) {

    const value = observable({
      foo: 0,
      bar: 0,
      get condition() {
        return this.foo >= 0;
      },
    })

    autorun(() => {
      console.log('value foo is :' + value.condition);

    });

    value.foo = 4;
    value.foo = -400;

    super(props);
  }

  render() {
    return (
      <div >mobx测试1</div>
    )
  }
}



export default MobxDemo
