'use strict'
import 'whatwg-fetch'
import React from 'react'
import { Provider } from 'mobx-react';
import {Route,BrowserRouter,Switch} from 'react-router-dom'

import MobxDemo from './mobxdemo'
import MobxDemo2 from './mobxdemo2'
import Home from './home'
import NotFound from './404'
import asyncComponent from '../common/component/async'
import commonStyle from '../common/css/css.css'
import stores from '../stores'
const userRouter = asyncComponent(() => System.import('./user/index').then(module => module.default))

class Component extends React.Component {
  render() {
    return (
      <Provider {...stores}>
  			<BrowserRouter>
          <div className ={commonStyle.container}>
            <Switch>
    				   <Route exact path="/" component={Home}></Route>
               <Route exact path="/mobxdemo" component={MobxDemo}></Route>
               <Route exact path="/mobxdemo2" component={MobxDemo2}></Route>
               <Route exact path="/user" component={userRouter}></Route>
               <Route component={NotFound} />
            </Switch>
          </div>
  			</BrowserRouter>
      </Provider>
    )
  }
}

export default Component
