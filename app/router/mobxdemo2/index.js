import {observable, autorun, computed, action} from 'mobx';
import React from 'react';
import {observer} from 'mobx-react';

@observer
export default class MobxDemo2 extends React.Component {

  @observable
  myvalue = {
    foo: 0
  }

  constructor(props) {

    super(props);

    this.myvalue.foo = 2;
    this.myvalue.foo = -200;
    this.myvalue.foo = 200;

    this.state = {
      username: 'yrs'
    }

    this.addValueByMobx = this.addValueByMobx.bind(this);
  }

  componentDidMount() {
    this.setState({
      username: 'lyn',
      age: 10
    });
  }

  @action
  addValueByMobx() {
    this.myvalue.foo++;
  }

  setAgeByState() {

    let age = this.state.age;
    age++;
    this.setState({
      age: age
    });
  }

  render() {

    return (
      <div style={{margin: '0 auto'}}>
        <div >mobx测试2</div>
        <div>result foo:{this.myvalue.foo}</div>
        <div>result age:{this.state.age}</div>
        <div>
          <input type="button" value="addByState" onClick={this.setAgeByState.bind(this)}/>
        </div>
        <div>
          <input type="button" value="addByMobx" onClick={this.addValueByMobx}/>
        </div>
      </div>
    )
  }
}
