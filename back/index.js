var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cors = require('cors');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies

//跨域
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });
//或者
app.use(cors());

app.get('/', function (req, res, next) {
  res.json(
    {data: '测试请求！', code: 0}
  )
})

//模块配置获取
app.get('/modulesetting/:id', function (req, res, next) {
  console.log(req.params.id);
  res.json(
    {
      id: req.params.id,
      data: {
        Modules: ['PurchaseOrder', 'OrderMag']
      }, code: 0
    }
  )
})

//模块配置保存
app.post('/modulesetting', function (req, res, next) {
  console.log(JSON.stringify(req.body));
  res.json(
    {data: '模块保存成功', code: 0}
  )
})

app.listen(3000, function () {
  console.log('访问：http://localhost:3000/')
})
